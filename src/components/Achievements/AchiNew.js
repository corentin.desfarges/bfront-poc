import React, {useState, useRef, useEffect} from 'react';
import {useDispatch} from "react-redux";

import {addAchievement, updateAchievement} from "../../actions/achievement";
import Button from "../ui/Button";
import Checkbox from "../ui/Checkbox";
import AddIcon from "../../assets/add.svg";
import CancelIcon from "../../assets/cancel.svg";

function AchiNew(props) {

  const titleRef = useRef();
  const descriptionRef = useRef();

  const {
    title: propTitle = "",
    description: propDescription = "",
    isDaily: propIsDaily = false,
    isWeekly: propIsWeekly = false,
    isMonthly: propIsMonthly = false,
    isOneShot: propIsOneShot = false,
  } = props;

  const [title, setTitle] = useState(propTitle);
  const [description, setDescription] = useState(propDescription);
  const [isDaily, setIsDaily] = useState(propIsDaily);
  const [isWeekly, setIsWeekly] = useState(propIsWeekly);
  const [isMonthly, setIsMonthly] = useState(propIsMonthly);
  const [isOneShot, setIsOneShot] = useState(propIsOneShot);
  const [isExpanded, setExpanded] = useState(false);
  const dispatch = useDispatch();

  const clearAll = () => {
    setExpanded(false);
    setTitle("");
    setDescription("");
    setIsDaily(false);
    setIsWeekly(false);
    setIsMonthly(false);
    setIsOneShot(false);
  };

  const cancelEdition = () => {
    props.cancelEdition();
  };

  const updateMode = !!props.uuid;

  const createAchievement = () => {
    if (updateMode) {
      dispatch(updateAchievement(props.uuid, {title, description, isDaily, isWeekly, isMonthly, isOneShot}));
    } else {
      dispatch(addAchievement({title, description, isDaily, isWeekly, isMonthly, isOneShot}));
    }
    clearAll();
  };

  useEffect(() => {
    titleRef.current.focus();
  }, []);

  const disableAll = () => {
    setIsOneShot(false);
    setIsDaily(false);
    setIsWeekly(false);
    setIsMonthly(false);
  };

  const handleKeyPressed = (input, e) => {
    if (e.charCode === 13) {
      if (input === "title") {
        descriptionRef.current.focus();
      } else if (input === "description") {
        createAchievement();
      }
    }
  }


  const [isBlock2Visible, setBlock2Visible] = useState(false);
  useEffect(() => {
    setBlock2Visible(isExpanded || title.length > 0);
  }, [title, isExpanded]);

  return (
    <div className={`offset-md-2 col-md-8 offset-lg-3 col-lg-6 nopadding newBlock ${isBlock2Visible ? "active" : ""}`}>
      <input
        ref={titleRef}
        className="title"
        placeholder="Je dois également penser à..."
        type="text"
        value={title}
        onChange={e => setTitle(e.target.value)}
        onKeyPress={(e) => handleKeyPressed("title", e)}
      /><br/>
      <img className={"cancel"} alt="cancel" src={CancelIcon} onClick={() => updateMode ? cancelEdition() : clearAll()}/>
      <img className={"add"} alt="add" src={AddIcon} onClick={() => setExpanded(true)}/>
      <div className={isBlock2Visible ? "showBlock2" : "hideBlock2"}>
        {/*<img className={"enter"} alt="enter" src={EnterIcon} onClick={() => setExpanded(true)}/>*/}
        <input
          ref={descriptionRef}
          className="description"
          placeholder="Description"
          type="text"
          value={description}onChange={e => setDescription(e.target.value)}
          onKeyPress={(e) => handleKeyPressed("description", e)}
        /><br/>
        <Checkbox checked={isOneShot} size="small" value={isOneShot} onClick={() => {disableAll(); setIsOneShot(!isOneShot);}}>Ponctuelle</Checkbox>
        <Checkbox checked={isDaily} size="small" value={isDaily} onClick={() => {disableAll(); setIsDaily(!isDaily);}}>Quotidienne</Checkbox>
        <Checkbox checked={isWeekly} size="small" value={isWeekly} onClick={() => {disableAll(); setIsWeekly(!isWeekly);}}>Hebdomadaire</Checkbox>
        <Checkbox checked={isMonthly} size="small" value={isMonthly} onClick={() => {disableAll(); setIsMonthly(!isMonthly);}}>Mensuelle</Checkbox>
        {/*<br/>*/}
        {/*<Button onClick={() => createAchievement()}>Enregister</Button>*/}
      </div>
    </div>
  );
}

export default AchiNew;
