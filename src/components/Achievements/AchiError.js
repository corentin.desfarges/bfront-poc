import React from 'react';

function AchiError() {

  return (
    <div>
      <h1>Something went wrong...</h1>
      <img src="https://media0.giphy.com/media/dX1K0xlyYZ4jFXfaMC/giphy.gif" alt="error"/>
    </div>
  );
}

export default AchiError;
