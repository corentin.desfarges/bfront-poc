import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import * as R from "ramda";

import {validateAchievement, removeAchievement} from "../../actions/achievement";

import DeleteIcon from '../../assets/delete.svg';
import EditIcon from '../../assets/edit.svg';

import './styles.css';
import AchiNew from "./AchiNew";
import Checkbox from "../ui/Checkbox";

function AchiItem(props) {

  const dispatch = useDispatch();

  const {isDaily, isWeekly, isMonthly, isOneShot, uuid, title, description, accomplishments} = props.item;



  const [isDone, setIsDone] = useState(R.pathEq([0, "status"], "VALIDATED", accomplishments));
  const [isEditing, setIsEditing] = useState(false);
  // const [isDeleted, setIsDeleted] = useState(false);

  const remove = (uuid) => {
    dispatch(removeAchievement(uuid));
    // setIsDeleted(true);
  };

  const handleCheckbox = () => {
    dispatch(validateAchievement(uuid, !isDone));
    setIsDone(!isDone);
  };

  const hasFooter = isDaily || isWeekly || isMonthly;

  if (isEditing) {
    return <div style={{marginBottom: 30, border: "0 solid red"}}>
      <AchiNew uuid={uuid} title={title} description={description} isDaily={isDaily} isWeekly={isWeekly} isMonthly={isMonthly} isOneShot={isOneShot} cancelEdition={() => setIsEditing(false)} />
    </div>
  }

  return (
    <div className={`achievement offset-md-2 col-md-8 offset-lg-3 col-lg-6 ${isDone ? 'validated' : ''}`}>
      <div className={"topBar"}>
        <div className={"actions left"}>
          <Checkbox size="big" checked={isDone} onClick={handleCheckbox}/>
        </div>
        <h3>{title}</h3>
        <div className={"actions right"}>
          <img className={"edit"} alt="edit" src={EditIcon} onClick={() => setIsEditing(!isEditing)}/>
          <img className={"delete"} alt="delete" src={DeleteIcon} onClick={() => remove(uuid)}/>
        </div>
      </div>
      {description &&
      <p className={"description"}>{description}</p>
      }
      {hasFooter &&
      <div className="footer">
        {(isDaily || isWeekly || isMonthly) && "Action "}
        {isDaily && "quotidienne"}
        {isWeekly && "hebdomadaire"}
        {isMonthly && "mensuelle"}
      </div>
      }
    </div>
  );
}

export default AchiItem;
