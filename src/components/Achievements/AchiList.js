import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import * as R from "ramda";

import {getAchievements,} from "../../actions/achievement";
import AchiError from "./AchiError";
import AchiItem from "./AchiItem";
import AchiNew from "./AchiNew";

function AchiList(props) {

  const dispatch = useDispatch();
  const token = useSelector(R.path(["userReducer", "token"]));
  const {list, error, loading} = useSelector((s) => s.achievementReducer);

  useEffect(() => {
    dispatch(getAchievements({limit: props.limit}));
  }, []);

  return (

    <div className="achievements col-12">
      {error &&
        <AchiError/>
      }
      {/*{loading &&*/}
      {/*  <img src="https://media0.giphy.com/media/GjqJ7JtXZlgdi/giphy.gif" alt="loading"/>*/}
      {/*}*/}
      {/*{!loading &&*/}
        <div>
          <AchiNew />
          {list.map((item) => <AchiItem item={item} key={item.uuid} />)}
        </div>
      {/*}*/}
    </div>
  );
}

export default AchiList;
