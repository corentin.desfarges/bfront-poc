import React from 'react';

import Input from "./Input";

function Password(props) {
  return (
    <Input type={"password"} {...props} />
  );
}

export default Password;
