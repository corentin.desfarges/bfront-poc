import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";

import * as R from "ramda";

import AchiList from "./Achievements/AchiList";
import AchiNew from "./Achievements/AchiNew";
import DayView from "./Achievements/DayView";
import AchiMenu from "./Achievements/AchiMenu";
import Login from "./Account/Login";
import Signup from "./Account/Signup";

function App() {
  const token = useSelector(R.path(["userReducer", "token"]));

  return (
    <div className="App">
      <h1>React Cats POC</h1>
      <h2>© 2020 - MeiKorporation 🐾</h2>
      {token &&
        <div>
          <AchiMenu/>
          <Switch>
            <Route path="/new" component={AchiNew}/>
            <Route path="/all" component={AchiList}/>
            <Route path="/daily" component={DayView}/>
            <Redirect to="/all" />
          </Switch>
        </div>
      }
      {!token &&
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route path="/signup" component={Signup}/>
          <Redirect to="/" />
        </Switch>
      }
    </div>
  );
}

export default App;
