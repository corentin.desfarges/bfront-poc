import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import * as R from "ramda";

import {getCats, releaseCat} from "../../actions/cat";
import CatCentCat from "./CatCentCat";
import CatItem from "./CatItem";

import './styles.css';

function CatList() {

  const dispatch = useDispatch();
  const token = useSelector(R.path(["userReducer", "token"]));
  const {list, error, loading} = useSelector((s) => s.catReducer);

  useEffect(() => {
    dispatch(getCats(token));
  }, [token]);

  return (
    <div className="cats">
      {error &&
        <CatCentCat/>
      }
      {loading &&
        <img src="https://media.giphy.com/media/fiBlgzowrS9i/giphy.gif" alt="loading"/>
      }
      {!loading &&
        list.map((cat) => <CatItem item={cat} key={Math.random()} />)
      }
    </div>
  );
}

export default CatList;
