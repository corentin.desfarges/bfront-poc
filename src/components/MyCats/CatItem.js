import React, {useState} from 'react';
import {useDispatch} from "react-redux";

import {releaseCat} from "../../actions/cat";

import './styles.css';

function CatItem(props) {

  const dispatch = useDispatch();
  const [isDeleted, setIsDeleted] = useState(false);

  const remove = (id) => {
    dispatch(releaseCat(id));
    setIsDeleted(true);
  };

  const cat = props.item;
  return (
    <span className="cat">
      <img alt="" src={cat.img} onClick={() => remove(cat.uuid)} className={isDeleted ? "deleted": ""}/>
    </span>
  );
}

export default CatItem;
