import React from 'react';

import './styles.css';

function CatCentCat() {

  return (
    <div>
      <h1>Something went wrong...</h1>
      <img src="https://media.giphy.com/media/8zFzUSzVZr7X2/giphy.gif" alt="error"/>
    </div>
  );
}

export default CatCentCat;
