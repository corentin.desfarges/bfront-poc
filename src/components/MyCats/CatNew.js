import React, {useState} from 'react';
import {useDispatch} from "react-redux";

import {addCat} from "../../actions/cat";

function CatNew() {

  const [inputName, setInputName] = useState('');
  const [inputImg, setInputImg] = useState('');
  const dispatch = useDispatch();

  const createCat = () => {
    dispatch(addCat({inputName, inputImg}));
  };

  const createRandomCat = () => {
    for (let i = 0 ; i < 1 ; i++) {
      const number = Math.floor(Math.random()*403) + 1;
      dispatch(addCat({inputName: `Cat #${number}`, inputImg: `https://rand.cat/gifs/cat-${number}.gif`}));
    }
  };

  return (
    <div>
      <input placeholder="name" type="text" value={inputName} onChange={e => setInputName(e.target.value)} /><br/>
      <input placeholder="image" type="text" value={inputImg} onChange={e => setInputImg(e.target.value)} /><br/>
      <button onClick={() => createCat()}>Poster le chat</button>
      <button onClick={() => createRandomCat()}>RandomCat</button>
    </div>
  );
}

export default CatNew;
