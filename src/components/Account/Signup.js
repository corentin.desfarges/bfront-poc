import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {register} from '../../actions/auth';

import Input from "../ui/Input";
import Link from "../ui/Link";

function SignUp() {

  const dispatch = useDispatch();
  const createdUser = useSelector(s => s.userReducer.lastCreatedUser);

  const [inputEmail, setInputEmail] = useState('');
  const [inputPassword, setInputPassword] = useState('');

  return (
    <div className="SignUp">
      <div className="row" style={{margin: 20}}>
        <div className="offset-4 col-4">
          {!createdUser &&
          <div>
            <Input type="text" id="name" placeholder="Identifiant" value={inputEmail} onChange={text => setInputEmail(text)} />
            <Input type="password" id="name" placeholder="Mot de passe" value={inputPassword} onChange={text => setInputPassword(text)} />
            <button onClick={() => dispatch(register(inputEmail, inputPassword))}>Créez votre compte !</button>
          </div>
          }
          {createdUser &&
          <div>
            Bienvenue {createdUser.username} !
            <br/>
            <Link to="/">Connectez-vous !</Link>
          </div>
          }
        </div>
      </div>
    </div>
  );
}

export default SignUp;
