import React, {useState} from 'react';
import {useDispatch} from "react-redux";

import {login} from '../../actions/auth';
import SocialLogin from "./SocialLogin";

import Input from "../ui/Input";
import Button from "../ui/Button";
import Link from "../ui/Link";

function Login() {

  const dispatch = useDispatch();

  const [inputEmail, setInputEmail] = useState('');
  const [inputPassword, setInputPassword] = useState('');

  return (
    <div className="Login">
      <div className="row" style={{margin: 20}}>
        <div className="offset-4 col-4">
          <Input type="text" id="name" placeholder="Identifiant" value={inputEmail} onChange={text => setInputEmail(text)} />
          <Input type="password" id="name" placeholder="Mot de passe" value={inputPassword} onChange={text => setInputPassword(text)} />
          <br/>
          <Button onClick={() => dispatch(login(inputEmail, inputPassword))}>Se connecter</Button>
        </div>
      </div>
      <div>
        <SocialLogin/>
      </div>
      <hr />
      <Link to="/signup">Créez votre compte !</Link>
    </div>
  );
}

export default Login;
