import React from 'react';
import {useDispatch} from "react-redux";

import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import {GoogleLogin} from 'react-google-login';

import {loginOAuth} from "../../actions/auth";

import config from '../../config.js';
import GoogleLogo from '../../assets/google.png';
import FacebookLogo from '../../assets/facebook.png';

import "./styles.css";

function SocialLogin() {

  const dispatch = useDispatch();

  const onFailure = (error) => {
    console.error(error)
  };

  return (
    <div className="socialLogin">
      <div className="network">
        <p>Se connecter avec</p>
      </div>
      <FacebookLogin
        appId={config.FACEBOOK_APP_ID}
        autoLoad={false}
        callback={(r) => dispatch(loginOAuth('facebook', r))}
        render={renderProps => (
          <img className="network" alt="FacebookLogo" src={FacebookLogo} onClick={renderProps.onClick}/>
        )}
      />
      <GoogleLogin
        clientId={config.GOOGLE_CLIENT_ID}
        onSuccess={(r) => dispatch(loginOAuth('google', r))}
        onFailure={onFailure}
        render={renderProps => (
          <img className="network" alt="GoogleLogo" src={GoogleLogo} onClick={renderProps.onClick}/>
        )}
      />
    </div>
  );
}

export default SocialLogin;
