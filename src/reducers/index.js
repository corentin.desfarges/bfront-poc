import UserReducer from "./user";
import CatReducer from "./cat";
import AchievementReducer from "./achievement";

export default {
  userReducer: UserReducer,
  catReducer: CatReducer,
  achievementReducer: AchievementReducer
};
