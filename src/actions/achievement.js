import axios from "axios";

import config from '../config.js';

export const getAchievements = ({limit, sort} = {}) => (dispatch) => {
  dispatch({type: "REQUEST_ACHIEVEMENTS"});
  axios.get(`${config.HOST}/api/goals`, {withCredentials: true, headers: {limit, sort: sort || "-createdAt"}})
    .then((res) => {
      dispatch({type: "RECEIVED_ACHIEVEMENTS", items: res.data})
    })
    .catch(() => {
      dispatch({type: "RECEIVED_ACHIEVEMENTS_ERROR"});
    })
};

export const addAchievement = ({title, description, isDaily = false, isWeekly = false, isMonthly = false, isOneShot = false}) => (dispatch) => {
  axios.post(`${config.HOST}/api/goals`, {title, description, isDaily, isWeekly, isMonthly, isOneShot}, {withCredentials: true})
    .then((response) => {
      dispatch(getAchievements());
    })
};

export const updateAchievement = (uuid, {title, description, isDaily = false, isWeekly = false, isMonthly = false, isOneShot = false}) => (dispatch) => {
  axios.put(`${config.HOST}/api/goals/${uuid}`, {title, description, isDaily, isWeekly, isMonthly, isOneShot}, {withCredentials: true})
    .then((response) => {
      dispatch(getAchievements());
    })
};

export const validateAchievement = (uuid, isDone) => (dispatch) => {
  axios.post(`${config.HOST}/api/goals/${uuid}/validate`, {status: isDone ? "VALIDATED" : "CANCELED"}, {withCredentials: true})
    .then((response) => {
      dispatch(getAchievements());
    })
};

export const removeAchievement = (catId) => (dispatch, getState) => {
  axios.delete(`${config.HOST}/api/goals/${catId}`, {headers: {Authorization: 'Bearer ' + getState().userReducer.token}})
    .then((response) => {
      dispatch(getAchievements());
    })
};
