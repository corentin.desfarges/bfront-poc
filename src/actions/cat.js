import axios from "axios";

import config from '../config.js';

const getCats = () => (dispatch, getState) => {
  dispatch({type: "REQUEST_CATS"});
  axios.get(`${config.HOST}/api/cats`, {withCredentials: true, headers: {Authorization: 'Bearer ' + getState().userReducer.token}})
    .then((res) => {
      dispatch({type: "RECEIVED_CATS", items: res.data})
    })
    .catch(() => {
      dispatch({type: "RECEIVED_CATS_ERROR"});
    })
};

const addCat = ({inputName, inputImg}) => (dispatch, getState) => {
  axios.post(`${config.HOST}/api/cats`, {name: inputName, img: inputImg}, {headers: {Authorization: 'Bearer ' + getState().userReducer.token}})
    .then((response) => {
      dispatch(getCats());
    })
};

const releaseCat = (catId) => (dispatch, getState) => {
  axios.delete(`${config.HOST}/api/cats/${catId}`, {headers: {Authorization: 'Bearer ' + getState().userReducer.token}})
    .then((response) => {
      // dispatch(getCats());
    })
};

export {
  addCat,
  getCats,
  releaseCat,
};
